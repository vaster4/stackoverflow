# 实验报告

实验环境准备

下载pwntools

![1702872034076](image/lab/1702872034076.png)

## level0

调用了sayhi

![1702871013477](image/lab/1702871013477.png)

sayhi：

![1702871398333](image/lab/1702871398333.png)

myread的作用是读一行最多max_len个字符的字符串，从stdin读入

![1702885838380](image/lab/1702885838380.png)

seeme函数会执行sh，想办法溢出执行seeme就可以了

![1702886060791](image/lab/1702886060791.png)

入口位于 0x080485b3

![1702892196058](image/lab/1702892196058.png)


## level1
